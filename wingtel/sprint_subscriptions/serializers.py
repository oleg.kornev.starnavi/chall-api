from rest_framework import serializers
from wingtel.sprint_subscriptions.models import SprintSubscription


class SprintSubscriptionSerializer(serializers.ModelSerializer):

    phone_number = serializers.CharField(required=True)
    device_id = serializers.CharField(required=True)

    def validate(self, attrs):
        if not self.instance is None:
            if not self.instance.status == self.instance.STATUS.new:
                raise serializers.ValidationError('Only subscription with a status of `new` can be change')
        return super().validate(attrs)

    class Meta:
        model = SprintSubscription
        fields = '__all__'
