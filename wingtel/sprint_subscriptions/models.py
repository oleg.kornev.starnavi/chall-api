from django.contrib.auth.models import User
from django.db import models

from rest_framework.exceptions import ValidationError

from model_utils import Choices

from wingtel.plans.models import Plan


class SprintSubscription(models.Model):
    """Represents a subscription with Sprint for a user and a single device"""
    STATUS = Choices(
        ('new', 'New'),
        ('active', 'Active'),
        ('suspended', 'Suspended'),
        ('expired', 'Expired'),
    )

    # Owning user
    user = models.ForeignKey(User, on_delete=models.PROTECT)

    plan = models.ForeignKey(Plan, null=True, on_delete=models.PROTECT)
    status = models.CharField(max_length=10, choices=STATUS, default=STATUS.new)

    device_id = models.CharField(max_length=20, blank=True, default='')
    phone_number = models.CharField(max_length=20, blank=True, default='')
    phone_model = models.CharField(max_length=128, blank=True, default='')

    sprint_id = models.CharField(max_length=16, null=True)

    effective_date = models.DateTimeField(null=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    deleted = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.status == self.STATUS.active:
            if not all([self.device_id, self.phone_number, self.plan]):
                raise ValidationError('Activation is not possible until the fields are filled:'
                                      ' phone number, device id, plan')
        super(SprintSubscription, self).save(*args, **kwargs)
