import json
from django.contrib.auth.models import User

from rest_framework.test import APIRequestFactory, force_authenticate, APITestCase
from wingtel.att_subscriptions.views import ATTSubscriptionViewSet, ATTSubscription, ATTSubscriptionSerializer


#base
factory = APIRequestFactory()
user = User.objects.get(username='hank_williams')
testing_subscription = ATTSubscription.objects.filter(user=user)
testing_response = ATTSubscriptionSerializer(testing_subscription, many=True).data
view = ATTSubscriptionViewSet.as_view(
    {'get' : 'list',
     'post':'create',
     'patch':'partial_update'}
)


#check permissions
request = factory.get('/api/att_subscriptions/', format='json')
force_authenticate(request, user=user)
response = view(request)
test_case = APITestCase()
test_case.assertEqual(response.data, testing_response)

#check bad request
test_json = {'device_id':'',
             'plan':None,
             'phone_number':''}

bad_post_request = factory.post('/api/att_subscriptions/',
                                json.dumps(test_json),
                                content_type='application/json')
force_authenticate(bad_post_request, user=user)
response = view(bad_post_request)
assert response.status_code == 400

#check create
test_json = {'device_id':'12412412412412412',
             'plan': 1,
             'phone_number' : '2131231231231231'}
good_request = factory.post('/api/att_subscriptions/',
                                json.dumps(test_json),
                                content_type='application/json')

force_authenticate(good_request, user=user)
response = view(good_request)
assert response.status_code == 201


#checking attempts to change the record if the status is not equal to "new"
test_json = {'device_id':'111',
             'plan': 3,
             'phone_number': '1111'}
path_request = factory.patch('/api/att_subscriptions/',
                                json.dumps(test_json),
                                content_type='application/json')
force_authenticate(path_request, user=user)
response = view(path_request, pk=2)
assert response.status_code == 400


#create and activation
test_json = {'device_id':'11111',
             'plan': '',
             'phone_number': '2131231231231231'}

not_active_request_create = factory.post('/api/att_subscriptions/',
                                json.dumps(test_json),
                                content_type='application/json')

force_authenticate(not_active_request_create, user=user)
response = view(not_active_request_create)
test_id = response.data.get('id')

#Not activate
test_json = {}
not_active_request = factory.post(f'/api/att_subscriptions/',
                                json.dumps(test_json),
                                content_type='application/json')
force_authenticate(not_active_request, user=user)
view = ATTSubscriptionViewSet.as_view(actions={'post': 'activate',
                                                'patch':'partial_update'},
                                                detail=True)
response = view(not_active_request, pk=int(test_id))


assert response.status_code == 400


#activate
test_json = {'device_id':'111',
             'plan': 3,
             'phone_number': '1111'}

path_request = factory.patch('/api/att_subscriptions/',
                                json.dumps(test_json),
                                content_type='application/json')

force_authenticate(path_request, user=user)
response = view(path_request, pk=int(test_id))

test_json = {}
not_active_request = factory.post(f'/api/att_subscriptions/',
                                json.dumps(test_json),
                                content_type='application/json')
force_authenticate(not_active_request, user=user)

response = view(not_active_request, pk=int(test_id))
assert response.data.get('status') == 'active'
assert response.status_code == 200
