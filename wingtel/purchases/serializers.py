from rest_framework import serializers
from wingtel.purchases.models import Purchase
from django.utils import timezone


class PurchaseSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        instance = super().create(validated_data)
        subscription = instance.att_subscription if instance.att_subscription \
            else instance.sprint_subscription
        if any([timezone.now() > subscription.effective_date, subscription == subscription.STATUS.expired]):
            instance.status = instance.STATUS.overdue
        instance.save()
        return instance

    def validate(self, attrs):
        if all([attrs.get('att_subscription'), attrs.get('sprint_subscription')]):
            raise serializers.ValidationError('You can choose only one subscription plan')
        if not any([attrs.get('att_subscription'), attrs.get('sprint_subscription')]):
            raise serializers.ValidationError('You need choose one subscription plan')
        return super().validate(attrs)

    class Meta:
        model = Purchase
        fields = '__all__'