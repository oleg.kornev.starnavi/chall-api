import json
from django.contrib.auth.models import User

from rest_framework.test import APIRequestFactory, force_authenticate, APITestCase
from wingtel.purchases.views import PurchaseViewSet, Purchase, PurchaseSerializer


factory = APIRequestFactory()

user = User.objects.get(username='hank_williams')


view = PurchaseViewSet.as_view({'post':'create'})

#Check choose only one subscription
test_json = {
             'amount': 9.99,
             'att_subscription': 1,
             'sprint_subscription': 1
            }

bad_request = factory.post('/api/purchases/',
                            json.dumps(test_json),
                            content_type='application/json')

force_authenticate(bad_request, user=user)
response = view(bad_request)
assert response.status_code == 400


#check create pourchase with  with a status of `overdue`
test_json = {
             'amount': 9.99,
             'att_subscription': 2,
            }

good_request = factory.post('/api/purchases/',
                            json.dumps(test_json),
                            content_type='application/json')
force_authenticate(good_request, user=user)
response = view(good_request)

assert response.status_code == 201
assert response.data.get('status') == 'overdue'